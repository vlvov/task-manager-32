package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectRemoveByIdRequest;
import ru.t1.vlvov.tm.dto.Request.TaskRemoveByIdRequest;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Remove task by Id.";

    @NotNull
    private final String NAME = "task-remove-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setTaskId(id);
        getTaskEndpointClient().removeById(request);
    }

}