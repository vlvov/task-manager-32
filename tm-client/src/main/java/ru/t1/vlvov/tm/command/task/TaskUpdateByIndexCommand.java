package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectUpdateByIndexRequest;
import ru.t1.vlvov.tm.dto.Request.TaskUpdateByIndexRequest;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Update task by Index.";

    @NotNull
    private final String NAME = "task-update-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setName(name);
        request.setDescription(description);
        request.setTaskIndex(index);
        getTaskEndpointClient().updateByIndex(request);
    }

}