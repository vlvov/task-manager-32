package ru.t1.vlvov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.ServerAboutRequest;
import ru.t1.vlvov.tm.dto.Request.ServerVersionRequest;
import ru.t1.vlvov.tm.dto.Response.ServerAboutResponse;
import ru.t1.vlvov.tm.dto.Response.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient {

    @SneakyThrows
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @SneakyThrows
    @NotNull
    public ServerVersionResponse getAbout(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getName());
        System.out.println(serverAboutResponse.getEmail());
        @NotNull final ServerVersionResponse serverVersionResponse = client.getAbout(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

}
