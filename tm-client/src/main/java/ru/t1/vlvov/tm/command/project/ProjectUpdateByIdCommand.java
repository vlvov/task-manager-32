package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectCreateRequest;
import ru.t1.vlvov.tm.dto.Request.ProjectGetByIdRequest;
import ru.t1.vlvov.tm.dto.Request.ProjectUpdateByIdRequest;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Update project by Id.";

    @NotNull
    private final String NAME = "project-update-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setName(name);
        request.setDescription(description);
        request.setProjectId(id);
        getProjectEndpointClient().updateById(request);
    }

}