package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectListRequest;
import ru.t1.vlvov.tm.dto.Request.TaskListByProjectIdRequest;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "List tasks by Project Id.";

    @NotNull
    private final String NAME = "task-list-by-project-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest();
        @Nullable final List<Task> tasks = getTaskEndpointClient().listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}