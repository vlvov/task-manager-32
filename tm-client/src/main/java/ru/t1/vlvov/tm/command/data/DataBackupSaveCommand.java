package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataBackupLoadRequest;
import ru.t1.vlvov.tm.dto.Request.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save backup data to base 64 file.";

    @NotNull
    public static final String NAME = "data-save-backup";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpointClient().saveDataBackup(request);
    }

}
