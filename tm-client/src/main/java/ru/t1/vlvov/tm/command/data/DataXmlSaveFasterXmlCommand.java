package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataXmlLoadJaxBRequest;
import ru.t1.vlvov.tm.dto.Request.DataXmlSaveFasterXmlRequest;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    private final String NAME = "data-save-xml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest();
        getDomainEndpointClient().saveDataXmlFasterXml(request);
    }

}