package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataBase64SaveRequest;
import ru.t1.vlvov.tm.dto.Request.DataBinaryLoadRequest;
import ru.t1.vlvov.tm.dto.Request.DataBinarySaveRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    private final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest();
        getDomainEndpointClient().loadDataBinary(request);
    }

}
