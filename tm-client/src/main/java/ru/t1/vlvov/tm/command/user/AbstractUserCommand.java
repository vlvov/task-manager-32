package ru.t1.vlvov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.client.UserEndpointClient;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected UserEndpointClient getUserEndpointClient() {
        return serviceLocator.getUserEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST Name: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
