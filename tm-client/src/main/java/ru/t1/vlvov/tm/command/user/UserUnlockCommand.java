package ru.t1.vlvov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.UserRemoveRequest;
import ru.t1.vlvov.tm.dto.Request.UserUnlockRequest;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-unlock";

    @NotNull
    private final String DESCRIPTION = "Unlock user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest();
        request.setLogin(login);
        getUserEndpointClient().unlockUser(request);
    }

}
