package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataJsonSaveFasterXmlRequest;
import ru.t1.vlvov.tm.dto.Request.DataJsonSaveJaxBRequest;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data to json file.";

    @NotNull
    private final String NAME = "data-save-json-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest();
        getDomainEndpointClient().saveDataJsonJaxB(request);
    }

}
