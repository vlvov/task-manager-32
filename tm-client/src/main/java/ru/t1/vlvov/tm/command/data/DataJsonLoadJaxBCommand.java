package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataJsonLoadFasterXmlRequest;
import ru.t1.vlvov.tm.dto.Request.DataJsonLoadJaxBRequest;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from json file.";

    @NotNull
    private final String NAME = "data-load-json-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest();
        getDomainEndpointClient().loadDataJsonJaxB(request);
    }

}
