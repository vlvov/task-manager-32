package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataBinaryLoadRequest;
import ru.t1.vlvov.tm.dto.Request.DataJsonLoadFasterXmlRequest;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from json file.";

    @NotNull
    private final String NAME = "data-load-json";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest();
        getDomainEndpointClient().loadDataJsonFasterXml(request);
    }

}
