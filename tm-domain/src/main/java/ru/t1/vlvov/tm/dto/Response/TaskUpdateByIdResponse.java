package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(Task task) {
        super(task);
    }

}
