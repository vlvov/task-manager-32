package ru.t1.vlvov.tm.dto.Response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ApplicationErrorResponse extends AbstractResultResponse{

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
