package ru.t1.vlvov.tm.dto.Response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Task;

@Getter
@Setter
public abstract class AbstractTaskResponse extends AbstractResponse {

    public AbstractTaskResponse(Task task) {
        this.task = task;
    }

    @Nullable
    private Task task;

}
