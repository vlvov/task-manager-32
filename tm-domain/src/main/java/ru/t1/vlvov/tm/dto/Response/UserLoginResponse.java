package ru.t1.vlvov.tm.dto.Response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
