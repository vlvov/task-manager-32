package ru.t1.vlvov.tm.dto.Request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserRegistryRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

}
