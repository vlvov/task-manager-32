package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(Task task) {
        super(task);
    }

}
