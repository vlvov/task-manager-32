package ru.t1.vlvov.tm.dto.Request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Sort;

@Getter
@Setter
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectListRequest() {
    }

    public ProjectListRequest(@Nullable final Sort sort) {
        this.sort = sort;
    }

}
