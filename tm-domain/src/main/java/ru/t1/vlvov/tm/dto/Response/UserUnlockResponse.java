package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.User;

public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(User user) {
        super(user);
    }
}
