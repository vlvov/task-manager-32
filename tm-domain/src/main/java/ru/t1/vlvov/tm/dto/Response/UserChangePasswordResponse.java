package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.User;

public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(User user) {
        super(user);
    }

}
