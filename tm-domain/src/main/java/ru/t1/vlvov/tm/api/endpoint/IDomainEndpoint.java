package ru.t1.vlvov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.*;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;

public interface IDomainEndpoint {

    @NotNull
    AbstractResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    AbstractResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    AbstractResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    AbstractResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    AbstractResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    AbstractResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    AbstractResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    AbstractResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    AbstractResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    AbstractResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    AbstractResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    AbstractResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    AbstractResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    AbstractResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    AbstractResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

    @NotNull
    AbstractResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

}
