package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.User;

public final class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(User user) {
        super(user);
    }
}
