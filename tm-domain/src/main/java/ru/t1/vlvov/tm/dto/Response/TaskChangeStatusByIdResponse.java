package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(Task task) {
        super(task);
    }

}
