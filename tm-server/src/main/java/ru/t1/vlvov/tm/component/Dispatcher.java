package ru.t1.vlvov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.endpoint.Operation;
import ru.t1.vlvov.tm.dto.Request.AbstractRequest;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    private Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(@NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation) {
        map.put(reqClass, operation);
    }

    @NotNull
    public AbstractResponse call(@NotNull final AbstractRequest request) {
        Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
