package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @Nullable
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @Nullable
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @NotNull String description);

    @Nullable
    Task updateByIndex(@Nullable String userId,
                       @Nullable Integer index,
                       @Nullable String name,
                       @NotNull String description);

}
