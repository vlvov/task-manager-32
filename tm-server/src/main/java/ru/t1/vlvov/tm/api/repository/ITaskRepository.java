package ru.t1.vlvov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, String projectId);

}
