package ru.t1.vlvov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator comparator);

}
