package ru.t1.vlvov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    final String FILE_NAME = "application.properties";

    @NotNull
    final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    final String SERVER_PORT_KEY = "server.port";

    @NotNull
    final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    final String PASSWORD_SECRET_DEFAULT = "5926";

    @NotNull
    final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    final String PASSWORD_ITERATION_DEFAULT = "3156";

    @NotNull
    final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    final String EMPTY_VALUE = "---";

    @NotNull
    final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return Integer.parseInt(getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        return Integer.parseInt(getStringValue(SERVER_PORT_KEY, EMPTY_VALUE));
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}
