package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.endpoint.Operation;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.dto.Request.AbstractRequest;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;
import ru.t1.vlvov.tm.task.AbstractServerTask;
import ru.t1.vlvov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public final class Server {

    @NotNull
    private ServerSocket serverSocket;

    @NotNull
    private final ExecutorService es = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @NotNull
    private final IServiceLocator serviceLocator;

    public Server(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        es.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final Integer port = serviceLocator.getPropertyService().getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(Class<RQ> reqClass, Operation<RQ, RS> operation) {
        dispatcher.registry(reqClass, operation);
    }

    @NotNull
    public AbstractResponse call(AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        serverSocket.close();
        es.shutdown();
    }

}
