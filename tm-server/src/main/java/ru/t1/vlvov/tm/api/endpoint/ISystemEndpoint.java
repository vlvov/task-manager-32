package ru.t1.vlvov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.ServerAboutRequest;
import ru.t1.vlvov.tm.dto.Request.ServerVersionRequest;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;

public interface ISystemEndpoint {

    @NotNull
    AbstractResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    AbstractResponse getVersion(@NotNull ServerVersionRequest request);

}
